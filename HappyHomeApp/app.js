
global.__rootdir = __dirname;
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var sassMiddleware = require('node-sass-middleware');
var mongoose = require('mongoose');
var indexRouter = require('./routes/index');
var healthRouter = require('./routes/health');
var usersRouter = require('./routes/users');
var homesRouter = require('./routes/homes');
var authRouter = require('./routes/auth');
var jurisdictionsRouter = require('./routes/jurisdictions');
var tasksRouter = require('./routes/tasks');
var landlordsRouter = require('./routes/landlords');
var config = require('config');
var bodyParser = require('body-parser');
var auth = require('./auth/auth');


mongoose.connect('mongodb+srv://' + config.get("MongoDB.username") + ':' + config.get("MongoDB.password") + '@' + config.get("MongoDB.clusterUrl") + '/' + config.get("MongoDB.dbName") + '?retryWrites=true&w=majority'
).catch(err => {throw err;});


var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/health', healthRouter); //used to test whether several elements of the api are up and running
app.use('/users', usersRouter);
app.use('/homes', homesRouter);
app.use('/auth', authRouter);
app.use('/jurisdictions', jurisdictionsRouter);
app.use('/landlords', landlordsRouter);
app.use('/tasks', tasksRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;
