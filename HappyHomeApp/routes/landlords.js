var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Landlord = require('../models/Landlord.js');
var auth = require(global.__rootdir + '/auth/auth');

//CREATE Landlords listing
router.post('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        Landlord.create(req.body)
        .then((landlord) => {
            res.status(201).json({"id": landlord._id}).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while creating the resource"]}).end();
        });
    }
]);

// GET Landlords listing.
router.get('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        var limit = req.query.limit ? parseInt(req.query.limit) : 25 //return 25 if no limit is set
    	Landlord.find(limit)
        .then((landlords) => {
    	  	res.status(200).json(landlords).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);

// READ single Landlord
router.get('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        Landlord.findOneById(req.params._id)
        .then((landlord) => {;
    	  	res.status(200).json(landlord).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);

// UPDATE Landlords listing
router.put('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        Landlord.updateOne(req.params._id,req.body)
        .then(() => {
        	res.status(204).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while updating the resource"]}).end();
        });
    }
]);

// DELETE Landlords listing
router.delete('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
    	Landlord.deleteOne(req.params._id)
        .then(() => {
    		res.status(204).end();
    	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while deleting the resource"]}).end();
        });
    }
]);

module.exports = router;