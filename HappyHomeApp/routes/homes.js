var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Home = require('../models/Home.js');
var auth = require(global.__rootdir + '/auth/auth');

//CREATE homes listing
router.post('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        Home.create(req.body)
        .then((home) => {
            res.status(201).json({"id": home._id}).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while creating the resource" + err]}).end();
        });
    }
]);

// GET homes listing.
router.get('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        var limit = req.query.limit ? parseInt(req.query.limit) : 25 //return 25 if no limit is set
    	Home.list(limit)
        .then((homes) => {
    	  	res.status(200).json(homes).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);

// READ single home
router.get('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        Home.findOneById(req.params._id)
        .then((home) => {;
    	  	res.status(200).json(home).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);

// UPDATE homes listing
router.put('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        Home.updateOne(req.params._id,req.body)
        .then(() => {
        	res.status(204).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while updating the resource"]}).end();
        });
    }
]);

// DELETE homes listing
router.delete('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
    	Home.deleteOne(req.params._id)
        .then(() => {
    		res.status(204).end();
    	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while deleting the resource"]}).end();
        });
    }
]);

module.exports = router;