var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();



/* GET health page. */
router.get('/', function(req, res, next) {
	res.send("OK").end();
});

router.get('/db', function(req, res, next) {
  
  if (mongoose.connection.readyState == 1) {
  	res.send("Connection OK").end();
  }
  else {
  	res.send("Connection Not OK").status(500).end();
  }
});

module.exports = router;
