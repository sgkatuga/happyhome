var express = require('express');
var router = express.Router();
var auth = require('../auth/auth');

router.post('/', [
  auth.verifyAuthFields,
  auth.verifyPasswordMatch,
  auth.login]);

module.exports = router;