var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/User.js');
var auth = require(global.__rootdir + '/auth/auth');

//CREATE users listing
router.post('/', function(req, res, next) {
    User.create(req.body,false)
    .then((user) => {
        res.status(201).json({"id": user._id});
    })
    .catch((err) => {
        res.status(500).json({"errors": ["Something went wrong while creating the resource (" + err.message +")"]}).end();
    });
});

//CREATE users listing
router.post('/admin', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        User.create(req.body,true)
        .then((user) => {
            res.status(201).json({"id": user._id});
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while creating the resource (" + err.message +")"]}).end();
        });
    }
]);

/*
//CREATE users listing
router.get('/:_id/email/verify', function(req, res, next) {
    User.verifyEmail(req.params._id,req.query.code)
    .then((user) => {
        res.status(301).redirect("/");
    })
    .catch((err) => {
        res.status(500).json({"errors": ["Something went wrong while creating the resource (" + err.message +")"]}).end();
    });
});*/

// GET users listing.
router.get('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        var limit = req.query.limit ? parseInt(req.query.limit) : 25 //return 25 if no limit is set
    	User.list(limit)
        .then((users) => {
    	  	res.status(200).json(users).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);


// READ single user
router.get('/:_id', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        User.findOneById(req.params._id)
        .then((user) => {
            res.status(200).json(user).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);

// READ a single user's homes
router.get('/:_id/homes', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        User.findOneById(req.params._id)
        .then((user) => {
            res.status(200).json(user.homes).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);

// 
router.put('/:_id/homes/:homeId/sublet', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        var isCurrent = (req.body.current == undefined ? false : req.body.current);
        User.subletHome(req.params._id,req.params.homeId,req.body._id)
        .then(() => {
            res.status(204).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while updating the resource"]}).end();
        });
    }
]);

// ADD a home to a single user
router.put('/:_id/homes', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        var isCurrent = (req.body.current == undefined ? false : req.body.current);
        User.addHome(req.params._id,req.body._id,isCurrent)
        .then(() => {
            res.status(204).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while updating the resource"]}).end();
        });
    }
]);

// READ a single user's homes
router.delete('/:_id/homes/:homeId', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        User.removeHome(req.params._id,req.params.homeId)
        .then(() => {
            res.status(204).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while updating the resource"]}).end();
        });
    }
]);

// UPDATE users listing
router.put('/:_id', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        User.updateOne(req.params._id,req.body)
        .then(() => {
        	res.status(204).send({});
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while updating the resource"]}).end();
        });
    }
]);

// DELETE users listing
router.delete('/:_id', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
    	User.deleteOne(req.params._id).then(() => {
    		res.status(204).send({});
    	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while deleting the resource"]}).end();
        });
    }
]);

module.exports = router;
