const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = require('mongoose').Schema;
const ObjectId = Schema.Types.ObjectId;
const Mixed = Schema.Types.Mixed;
const Home = require(global.__rootdir + '/models/Home');
var crypto = require('crypto');
var mailer = require(global.__rootdir + '/utils/email');
const userSchema = Schema({	
							firstName: {type: String, required: true}, 
							lastName: {type: String, required: true},
							username: String,
							email: {type: String, unique: true, required: true},
							//emailVerified: {type: Boolean, default: false},
							emailVerified: {type: Boolean, default: false},
							emailVerificationCode: String,
							//emailVerified: {type: Boolean, default: false},
							//emailVerificationCode: String,
							phone: {type: String, unique: true, required: true},
							homes: [{
								_id: ObjectId,
								current: Boolean,
								isSubletting: {type: Boolean, default: false},
								isSublet: {type: Boolean, default: false}
							}],
							permissionLevel: {type: Number, default: 1},
							passwordHash: {type: String, required: true},
							location: {
								lat: Number, //might need to be mongoose.Decimal128
								lon: Number //might need to be mongoose.Decimal128
							},
							university: {
								_id: ObjectId,
								verified: Boolean
							},
							renterScore: {type: Number, default: 500},
							transactions: [{
								transactionType: {type: String, enum: ['taskComplete', 'addHome', 'removeHome', 'subletHome']},
								completionTime: Date,
								value: Mixed
							}], //Add so we can evaluate renter score
							settings: {

							}
						});

/* global db */
var User = mongoose.model('User', userSchema, 'user');

module.exports.create = (body,isAdmin) => {
	return new Promise((resolve,reject) => {

		//delete fields that can only be set by us
		delete body.emailVerified;
		delete body.emailVerificationCode;

		//if user tries to give themselves permissions set them back
		if (isAdmin) {
			body.permissionLevel = 2;
		}
		else {
			body.permissionLevel = 1;
		}

		//create a pssword hash
	    var salt = crypto.randomBytes(16).toString('base64');
	    var hash = crypto.createHmac('sha512',salt)
	                                    .update(body.password)
	                                    .digest("base64");

	    body.passwordHash = salt + "$" + hash;
	    //Make sure the unencrypted password is not stored
	    delete body.password;

	    //create a email verification code
	    body.emailVerificationCode = crypto.randomBytes(16).toString('base64'); 

		var newUser = new User(body);

	    newUser.save(function(err,user) {
	    	if (err) reject(err);	
	    	/*
	    	mailer.sendEmailVerification(user._id,user.email,user.emailVerificationCode);
	    	*/
	    	resolve(user);
	    });
	});
};

module.exports.list = (limit) => {
	return new Promise((resolve,reject) => {
		console.log("Limit: " + limit);
		User.find().limit(limit).skip(0).exec((err,users) => {
			if (err) reject(err);
			users.forEach((user) => {
				user.passwordHash = undefined;
		       	user.__v = undefined;
		        user.id = user._id;
				user._id = undefined;
				console.log(user);
			});
		  	resolve(users);
  		});
	});	
}

module.exports.findOneById = (id) => {
	return new Promise((resolve,reject) => {
		User.findOne({_id: id},(err,user) => {
		  	if (err) reject(err);
	        user.passwordHash = undefined;
	       	user.__v = undefined;
	        user.id = user._id;
			user._id = undefined;
			console.log(user);
		  	resolve(user);
  		});
	});	
} 

module.exports.findOneByEmail = (email) => {
	return new Promise((resolve,reject) => {
		User.findOne({email: email},(err,user) => {
		  	if (err) reject(err);
	       	user.__v = undefined;
	        user.id = user._id;
			user._id = undefined;
			console.log(user);
		  	resolve(user);
  		});
	});	
} 

module.exports.find = (params) => {
	return new Promise((resolve,reject) => {
		User.find(params,(err,user) => {
		  	if (err) reject(err);
	       	user.__v = undefined;
	        user.id = user._id;
			user._id = undefined;
			console.log(user);
		  	resolve(user);
  		});
	});	
} 

module.exports.updateOne = (id,newUser) => {
	return new Promise((resolve,reject) => {

		//delete fields that can only be set by us
		delete body.emailVerified;
		delete body.emailVerificationCode;

		//if user tries to give themselves permissions set them back
		if (body.permissionLevel > 1 || body.permissionLevel > 1) {
			body.permissionLevel = 1;
		}

		User.updateOne({_id: id}, newUser, function(err,user) {
	    	if (err) reject(err);
	    	resolve();
    	});
	});	
} 

module.exports.addHome = (id,homeId,isCurrent,isSublet = false) => {
	return new Promise((resolve,reject) => {
		var newHome = {
			_id: homeId,
			current: isCurrent,
			isSublet: isSublet
		};
		User.findOne({_id: id}, function(err,user) {
	    	if (err) reject(err);	
	    	user.homes.push(newHome);
	    	user.save((err) => {
	    		if (err) reject(err);
	    		
	    		Home.findOneById(homeId)
	    		.then((err,home) => {
	    			home.renters.push({
	    				_id: id,
	    				current: true,
	    				occupying: true,
	    				startDate: new Date()
	    			})
	    			home.save((err) => {
						if (err) reject(err);
	    			});
	    		});
	    		.catch((err) => {reject(err)});	
	    		
	    		module.exports.addTransaction(id,'addHome',null);
	    	});
	    	resolve();
    	});

	});	
}

module.exports.subletHome = (id,homeId,subleteeId) => {
	return new Promise((resolve,reject) => {
		User.findOne({_id: id}, function(err,user) {
	    	if (err) reject(err);	
	    	user.homes.map((home) => home.isSubletting = home._id == homeId ? true : home.isSubletting);
	    	user.save((err) => {
	    		if (err) reject(err);
	    		module.exports.addTransaction(id,'subletHome',null);
	    	});
    	});
    	module.exports.addHome(subleteeId,homeId,true,true)
    	.catch((err) => {reject(err)});


    	Home.sublet(homeId,id,subleteeId)
    	.then(() => {
    		resolve()
    	})
    	.catch((err) => {reject(err)});
	});	
}  

module.exports.removeHome = (id,homeId) => {
	return new Promise((resolve,reject) => {
		User.findOne({_id: id}, function(err,user) {
	    	if (err) reject(err);
	    	
	    	user.homes = user.homes.filter((val,i,arr) => {
	    		console.log(val._id.toString() + " " + homeId);
	    		return val._id.toString() != homeId;
	    	});
	    	user.save((err) => {
	    		if (err) reject(err);
	    		module.exports.addTransaction(id,'removeHome',null);
	    	});
	    	resolve();
    	});
	});	
} 

module.exports.addTransaction = async (id,transactionType,val) => {
	return new Promise((resolve,reject) => {
		var transaction = {
			transactionType: transactionType,
			completionTime: new Date(),
			value: val 
		};

		User.findOne({_id: id}, function(err,user) {
	    	if (err) reject(err);
	    	user.transactions.push(transaction);
	    	user.save((err) => {
	    		if (err) reject(err);
	    		module.exports.evaluateRenterScore(id);
	    	});
	    	resolve();
    	});
	});
}

module.exports.updateLocation = (id, lat, long) => {
	return new Promise((resolve,reject) => {
		var location  = {
			latitude: lat,
			longitude: long
		};
		
		User.findOne({_id: id}, function(err,user) {
	    	if (err) reject(err);
	    	user.location = location;
	    	user.save((err) => {
	    		if (err) reject(err);
	    	});
	    	resolve();
    	});
	});
}

module.exports.evaluateRenterScore = async (id) => {
	return new Promise((resolve,reject) => {
		User.findOne({_id: id}, function(err,user) {
	    	if (err) reject(err);

	    	//TODO: come up with a way more sophisticated renter score calculation
	    	user.renterScore += 1;

	    	user.save((err) => {
	    		if (err) reject(err);
	    	});
	    	resolve();
    	});
	});
}
/*
module.exports.verifyEmail = (id,code) => {
	return new Promise((resolve,reject) => {
		User.findOne({_id:id}, function (err,user) {
			if (err) reject(err);
			if (code == user.emailVerificationCode) {
				user.emailVerified = true;
				user.save(function (err,user) {
					if (err) reject(err);
					resolve(user);
				});
			}
			else {
				reject(new Error("Email Verification Code incorrect"));
			}
		});
	});	
}
*/
module.exports.deleteOne = (id) => {
	return new Promise((resolve,reject) => {
		User.deleteOne({"_id": id}, function (err,user) {
			if (err) reject(err);
			resolve();
		});
	});	
} 




