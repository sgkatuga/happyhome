const mongoose = require('mongoose')
const Schema = require('mongoose').Schema;
const ObjectId = Schema.Types.ObjectId;
const Mixed = Schema.Types.Mixed;
const landlordSchema = Schema({
							name: String,
							isRentalCompany: Boolean,
							homeIds: [ObjectId],
							phone: String,
							email: String
						});

/* global db */
var Landlord = mongoose.model('Landlord', landlordSchema, 'landlord');

module.exports.create = (body) => {
	return new Promise((resolve,reject) => {
		var newLandlord = new Landlord(body);
	    newLandlord.save(function(err,landlord) {
	    	if (err) reject(err);
	    	resolve(landlord);
	    });
	});
};

module.exports.find = (limit) => {
	return new Promise((resolve,reject) => {
		Landlord.find().limit(limit).exec((err,landlords) => {
			landlords.forEach((l) => {
				l.__v = undefined;
			    l.id = l._id;
				l._id = undefined;
			});
		  	if (err) reject(err);
		  	resolve(landlords);
  		});
	});	
}

module.exports.findOneById = (id) => {
	return new Promise((resolve,reject) => {
		Landlord.findOne({_id: id},(err,landlord) => {
	       	landlord.__v = undefined;
	        landlord.id = landlord._id;
			landlord._id = undefined;
		  	if (err) reject(err);
		  	resolve(landlord);
  		});
	});	
}

module.exports.updateOne = (id,newLandlord) => {
	return new Promise((resolve,reject) => {
		Landlord.updateOne({_id: id}, newLandlord, function(err,landlord) {
	    	if (err) reject(err);
	    	resolve();
    	});
	});	
} 

module.exports.deleteOne = (id) => {
	return new Promise((resolve,reject) => {
		Landlord.deleteOne({_id: id}, function (err) {
			if (err) reject(err);
			resolve();
		});
	});	
} 