'use strict';
const mongoose = require('mongoose')
const Schema = require('mongoose').Schema;
const ObjectId = Schema.Types.ObjectId;
const Mixed = Schema.Types.Mixed;
const taskSchema = Schema({
							completed: {type: Boolean, default: false},
							title: String,
							description: String,
							default: Boolean,
							requirements: [{
								resourceType: {type: String, enum: ['photo','url','email','document','number','string','date']},
								title: String,
								description: String,
								value: Mixed,
								provided: {type: Boolean, default: false}
							}]
						});

/* global db */
var Task = mongoose.model('Task', taskSchema, 'task');

module.exports.create = (body) => {
	return new Promise((resolve,reject) => {
		var newTask = new Task(body);
	    newTask.save(function(err,task) {
	    	if (err) reject(err);
	    	resolve(task);
	    });
	});
};

module.exports.list = (limit) => {
	return new Promise((resolve,reject) => {
		module.exports.find({},limit)
		.then((tasks) => {
		  	resolve(tasks);
	  	})
	    .catch((err) => {
	        reject(err);
	    });
	});	
}

module.exports.listDefault = (limit) => {
	return new Promise(async (resolve,reject) => {
		module.exports.find({default: true},limit)
		.then((tasks) => {
		  	resolve(tasks);
	  	})
	    .catch((err) => {
	        reject(err);
	    });
	});	
}

module.exports.find = async (params,limit = 25) => {
	return new Promise(async (resolve,reject) => {
		await Task.find(params).limit(limit).exec((err,tasks) => {
			if (err) reject(err);
			if (tasks) {
				tasks.forEach((j) => {
					j.__v = undefined;
				    j.id = j._id;
					j._id = undefined;
				});
			}
		  	resolve(tasks);
  		});
	});	
}

module.exports.findOneById = (id) => {
	return new Promise((resolve,reject) => {
		module.exports.find({_id: id},1)
		.then((tasks) => {
		  	resolve(tasks);
	  	})
	    .catch((err) => {
	        reject(err);
	    });
	});	
	
}

module.exports.updateOne = (id,newTask) => {
	return new Promise((resolve,reject) => {
		Task.updateOne({_id: id}, newTask, function(err,task) {
	    	if (err) reject(err);
	    	resolve();
    	});
	});	
} 

module.exports.deleteOne = (id) => {
	return new Promise((resolve,reject) => {
		Task.deleteOne({_id: id}, function (err) {
			if (err) reject(err);
			resolve();
		});
	});	
} 