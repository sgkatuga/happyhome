'use strict';
const mongoose = require('mongoose')
const Schema = require('mongoose').Schema;
const ObjectId = Schema.Types.ObjectId;
const Jurisdiction = require(global.__rootdir + '/models/Jurisdiction');
const Task = require(global.__rootdir + '/models/Task');
const Mixed = Schema.Types.Mixed;
const leaseSchema = Schema({
							_id: ObjectId,
							homeId: ObjectId,
							landlord: ObjectId,
							owner: ObjectId,
							extends: ObjectId,
							tenants: [{
								_id: ObjectId,
								primary: {type: Boolean, default: false}
							}],
							terms: { //TODO: figure out what parts of a lease we need to add
								rent: Number,
								paymentPeriod: {type: String, default: 'monthly', enum: ['daily','weekly','bi-weekly','monthly','quarterly','yearly']},
								startDate: Date,
								endDate: Date
							}
						});

/* global db */
var Lease = mongoose.model('Lease', leaseSchema, 'lease');

module.exports.create = (body) => {
	return new Promise(async (resolve,reject) => {
		var newLease = new Lease(body);
	    newLease.save(function(err,lease) {
	    	if (err) reject(err);
	    	resolve(lease);
	    });
	});
};

module.exports.find = (limit) => {
	return new Promise((resolve,reject) => {
		Lease.find().limit(limit).exec((err,leases) => {
			leases.forEach((lease) => {
				lease.__v = undefined;
			    lease.id = lease._id;
				lease._id = undefined;
			});
		  	if (err) reject(err);
		  	resolve(leases);
  		});
	});	
}

module.exports.findOneById = (id) => {
	return new Promise((resolve,reject) => {
		Lease.findOne({_id: id},(err,lease) => {
	       	lease.__v = undefined;
	        lease.id = lease._id;
			lease._id = undefined;
		  	if (err) reject(err);
		  	resolve(lease);
  		});
	});	
}

module.exports.updateOne = (id,newLease) => {
	return new Promise((resolve,reject) => {
		Lease.updateOne({_id: id}, newLease, function(err,lease) {
	    	if (err) reject(err);
	    	resolve();
    	});
	});	
} 

module.exports.deleteOne = (id) => {
	return new Promise((resolve,reject) => {
		Lease.deleteOne({_id: id}, function (err) {
			if (err) reject(err);
			resolve();
		});
	});	
}

module.exports.sublet = (id,userId,subletteeId) => {
	return new Promise((resolve,reject) => {
		Lease.findOne({_id: id}, (err,lease) => { //make sure to find the unsubletted Lease
			if (err) reject(err);

			Home.findOne({_id: id, occupying = false}, (err,home) => {
				if(err) reject (err);

				//add new renter in lease
				home.renters.push({
					_id: subletteeId,
					current: true,
					occupying: true,
					startDate: new Date()
				});

				//set subletting renter to not occupying
				home.renters.find((renter) => {
					return enter._id == userId;
				}).occupying = false;

				home.save((err) => {
					if (err) reject(err);
				});
			});

			//should edit existing lease

			// var sublet = new Lease({
			// 	homeId: lease.homeId,
			// 	terms: lease.terms,
			// 	tenants: [{
			// 		_id: subletteeId,
			// 		primary: true
			// 	}],
			// 	landlord: userId,
			// 	extends: id
			// });

			lease.tenants.push({
				_id: subletteeId,
				primary = true
			});

			lease.tenants.find((tenant) =>{
				return tenant._id == userId
			}).primary = false;

			lease.save((err) => {
				if (err) reject(err);
				resolve();
			});
		});
	});	
}