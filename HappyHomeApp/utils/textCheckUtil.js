var textCheckUtil = (function(){

    return {
       checkEmail: function (email) {
           var emailExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
           return emailExpression.test(String(email).toLowerCase());
       },
       checkAddress: function (address) {
            var characters = [...address];
            var containsSpecialCharacter = false;
            characters.forEach(function(character){
                if ( ",#-/!@$%^*(){}|[]\\".indexOf(character) >= 0){
                    containsSpecialCharacter = true;
                }
            })
            return !containsSpecialCharacter;
       },
       checkPhoneNumber: function (phoneNumber) {
           var phoneNumberExpression = ^(\+\d{1,2}\s)?\d{3}?[\s-]\d{3}[\s-]\d{4}$;
           return phoneNumberExpression.test(phoneNumber);
       }
    }
   
   }())