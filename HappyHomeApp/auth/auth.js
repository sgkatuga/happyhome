var express = require('express');
var crypto = require('crypto');
var path = require('path')
var User = require(global.__rootdir + '/models/User.js');
var jwt = require('jsonwebtoken');
var fs = require('fs');
const jwtSecret = fs.readFileSync(global.__rootdir + '/auth/private.key');

module.exports.level = {
  USER: 1,
  ADMIN: 2
}

module.exports.verifyJWTToken = (req, res, next) => {
    console.log(req.headers);
    if (req.headers['authorization']) {
        try {
            let authorization = req.headers['authorization'].split(' ');
            if (authorization[0] !== 'Bearer') {
                return res.status(401).send();
            } else {
                req.jwt = jwt.verify(authorization[1], jwtSecret);
                return next();
            }
        } catch (err) {
            return res.status(403).send();
        }
    } else {
        return res.status(401).send();
    }
};

module.exports.verifyAuthorization = (levelRequired) => {
  return (req,res,next) => {
    if (!req.jwt) {
      res.status(403).send({"errors": ["Token not provided"]});
    }
    else if(req.jwt.permissionLevel < levelRequired) {
      res.status(401).send({"errors": ["Unauthorized request"]});
    }
    else {
      console.log("Token good");
      next();
    }

  }
};

module.exports.verifyAuthFields = (req, res, next) => {
   if (req.body.password && req.body.password) {
    return next();
   }
   else {
    return res.status(400).send({errors: ['Email or password not provided']}).end();
   }
};

module.exports.verifyPasswordMatch = (req, res, next) => {
   User.findOneByEmail(req.body.email)
       .then((user)=>{
           if(!user) {
               res.status(404).send({});
           }
           else {
               var passwordFields = user.passwordHash.split('$');
               var salt = passwordFields[0];
               var hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
               console.log("Hash: " + hash + " Password hash: " + passwordFields[1])
               if (hash === passwordFields[1]) {
                   req.body = {
                       id: user._id,
                       email: user.email,
                       permissionLevel: user.permissionLevel,
                       provider: 'email',
                       name: user.firstName + ' ' + user.lastName
                   };
                   return next();
               } else {
                   return res.status(400).send({errors: ['Invalid email or password']}).end();
               }
           }
       });
};



module.exports.login = (req, res) => {
   try {
       let refreshId = req.body.id + jwtSecret;
       let salt = crypto.randomBytes(16).toString('base64');
       let hash = crypto.createHmac('sha512', salt).update(refreshId).digest("base64");
       req.body.refreshKey = salt;
       let token = jwt.sign(req.body, jwtSecret);
       let b = new Buffer(hash);
       let refresh_token = b.toString('base64');
       res.status(201).send({accessToken: token, refreshToken: refresh_token});
   } catch (err) {
       res.status(500).send({errors: err});
   }
};